# Neovim Config

My personal neovim configuration. Based on [kickstart.nvim](https://github.com/nvim-lua/kickstart.nvim).

## Installation

Repository content has to be placed in ~/.config/nvim/
